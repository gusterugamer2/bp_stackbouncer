﻿using Blastproof.Systems.Core;
using Blastproof.Systems.Core.Variables;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScoreManager" , menuName = "ScoreManager")]
public class SO_ScoreManager : ScriptableObject
{
    [SerializeField] private IntVariable _rNumOfPieces;
    [SerializeField] private FloatVariable _rProgressPercent;
    [SerializeField] private SimpleEvent _onStackCreated;
    [SerializeField] private SimpleEvent _onReplayClick;

    [ShowInInspector] private int _startNumOfPieces = 0;
    [ShowInInspector] private int _currentNumOfPieces = 0; 

    private void OnEnable()
    {
        _rNumOfPieces.onValueChanged += ChangeScore;
        _onStackCreated.Subscribe(Init);
        _onReplayClick.Subscribe(Reset);       
    }

    private void OnDisable()
    {
        _rNumOfPieces.onValueChanged -= ChangeScore;
        _onStackCreated.Unsubscribe(Init);
        _onReplayClick.Unsubscribe(Reset);      
    }

    private void Reset()
    {
        _startNumOfPieces = 0;
        _currentNumOfPieces = 0;
    }

    private void Init()
    {
        _startNumOfPieces = _rNumOfPieces.Value;
        Debug.Log(_startNumOfPieces);
        _currentNumOfPieces = _startNumOfPieces;
        //_rProgressPercent.Value = 0;
    }

    private void ChangeScore()
    {
        _currentNumOfPieces = _rNumOfPieces.Value;
        Debug.Log(_startNumOfPieces);
        _rProgressPercent.Value = _startNumOfPieces > 0 ? ((float)_currentNumOfPieces / _startNumOfPieces) : 0f;
    }

}
