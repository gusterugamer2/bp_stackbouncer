﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particles : MonoBehaviour
{
    private ParticleSystem _partSys;

    private void Awake()
    {
        _partSys = GetComponent<ParticleSystem>();
        Destroy(gameObject, _partSys.main.duration + 0.1f);
    }  

}
