﻿using Blastproof.Systems.Core;
using Blastproof.Systems.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Gameplay : MonoBehaviour
{
    [SerializeField] private UIState _gameplayState;
    [SerializeField] private SimpleEvent _onStart;

    private void OnEnable()
    {
        _onStart.Subscribe(_gameplayState.Activate);
    }

    private void OnDisable()
    {
        _onStart.Unsubscribe(_gameplayState.Activate);
    }
}
