﻿using Blastproof.Systems.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Start : MonoBehaviour
{
    [SerializeField] private UIState _startState;

    private void Start()
    {
        _startState.Activate();
    }
}
