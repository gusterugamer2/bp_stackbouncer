﻿using Blastproof.Systems.Core;
using Blastproof.Systems.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_ReplayButton : MonoBehaviour
{
    [SerializeField] private SimpleEvent _onReplayClick;
    [SerializeField] private UIState _stateGamePlay;

    public void Replay()
    {
        _onReplayClick.Invoke();
        _stateGamePlay.Activate();
    }
}
