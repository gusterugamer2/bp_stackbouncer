﻿using Blastproof.Systems.Core;
using Blastproof.Systems.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_GameOver : MonoBehaviour
{
    [SerializeField] private UIState _gameOverState;
    [SerializeField] private SimpleEvent _onGameOver;

    private void OnEnable()
    {
        _onGameOver.Subscribe(_gameOverState.Activate);
    }

    private void OnDisable()
    {
        _onGameOver.Unsubscribe(_gameOverState.Activate);
    }
}
