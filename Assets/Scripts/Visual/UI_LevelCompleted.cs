﻿using Blastproof.Systems.Core;
using Blastproof.Systems.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_LevelCompleted : MonoBehaviour
{
    [SerializeField] private SimpleEvent _onLevelCompleted;
    [SerializeField] private UIState _levelCompletedState;

    private void OnEnable()
    {
        _onLevelCompleted.Subscribe(_levelCompletedState.Activate);
    }

    private void OnDisable()
    {
        _onLevelCompleted.Unsubscribe(_levelCompletedState.Activate);
    }
}
