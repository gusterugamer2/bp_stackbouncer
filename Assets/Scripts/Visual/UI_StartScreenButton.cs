﻿using Blastproof.Systems.Core;
using Blastproof.Systems.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_StartScreenButton : MonoBehaviour
{
    [SerializeField] private SimpleEvent _onStart;      

    public void StartGame()
    {
        _onStart.Invoke();
    }
}
