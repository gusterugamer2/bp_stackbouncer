﻿using Blastproof.Systems.Core;
using Blastproof.Systems.Core.Variables;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class Stack : MonoBehaviour
{
    [SerializeField] private GameObject _spawnPoint;
    [SerializeField] private SimpleEvent _onStackCreated;
    [SerializeField] private SimpleEvent _onReplayClick;
    [SerializeField] private SimpleEvent _onGameOver;
    [SerializeField] private SimpleEvent _onStart;
    [SerializeField] private IntVariable _rNumOfPieces;
   
    private Texture2D _colors;
    private GameObject _stackPiece;
    private List<GameObject> _objectsInStack = new List<GameObject>();
    private List<KeyValuePair<int, int>> _blackPiecesRages = new List<KeyValuePair<int, int>>();
    private float _lastPieceHeight = 0.0f;   
    private int _nextPieceToPush = 0;
    private const float _COOLDOWN_TIME = 0.05f;
    private float _lastPushTime = 0.0f;        
    private Camera _mainCam;
    private HashSet<int> _badPieces = new HashSet<int>();

    private const float _PIECE_HEIGHT = 0.036f;
    private const float _CAMERA_OFFSET = 2.75f;

    private const int _NUM_OF_PIECES = 80;
    private const int _NUM_OF_BAD_PIECES = 13;
    private const int _NUM_OF_PIECES_EXCLUDED_FROM_END = 2;    

    private int _numOfRanges = 0;
    private const int _MAX_NUM_OF_BAD_PIECES_IN_RANGE = 6;
    private const int _MIN_NUM_OF_BAD_PIECES_IN_RANGE = 4;

    private const int _COLOR_OFFSET = 72;  

    private const int _MIN_OFFSET_TO_NEXT_RANGE = 8;
    private const int _MAX_OFFSET_TO_NEXT_RANGE = 15;

    private Color _bottomColor;
    private Color _topColor;

    private bool _isEnabled = false;

    private int _lastStartIndex = 0;

    private GameObject _spObj;   

    public List<GameObject> objectsInStack { get => _objectsInStack; }

    private void Awake()
    {
        _numOfRanges = (_NUM_OF_PIECES / 10) - 1;        
    }

    private void Start()
    {
        _mainCam = Camera.main;
        _spObj = _spawnPoint;
        _stackPiece = Resources.Load("Prefabs/StackPiece") as GameObject;
        _colors = Resources.Load("Textures/Colors") as Texture2D;   
        _lastPieceHeight = _spawnPoint.transform.position.y - _PIECE_HEIGHT/2;
        CreateStack();        
    }

    private void OnEnable()
    {
        _onReplayClick.Subscribe(CreateStack);
        _onStart.Subscribe(Enable);
        _onReplayClick.Subscribe(Enable);
        _onGameOver.Subscribe(Disable);
    }
    private void OnDisable()
    {
        _onReplayClick.Unsubscribe(CreateStack);
        _onReplayClick.Unsubscribe(Enable);
        _onGameOver.Unsubscribe(Disable);
        _onStart.Unsubscribe(Enable);
    }

    private void FixedUpdate()
    {
       if (Input.GetMouseButton(0) && Time.unscaledTime >= _lastPushTime + _COOLDOWN_TIME && _isEnabled)
       {
            PushNext();
            _lastPushTime = Time.unscaledTime;
       }
    }

    private void MoveCam()
    {
        if (_nextPieceToPush + 5 < _objectsInStack.Count)
        {           
            Vector3 destination = new Vector3(_mainCam.transform.position.x, _objectsInStack[_nextPieceToPush + 5].transform.position.y + _CAMERA_OFFSET, _mainCam.transform.position.z);
            _mainCam.transform.DOMove(destination, 1);
        }
    }

    private void CreateStack()
    {
        _blackPiecesRages.Clear();
        LevelColors();
        PickRangesInStack(out int pieciesBeforeLimit);

        foreach (GameObject go in _objectsInStack)
        {
            Destroy(go);
        }

        _objectsInStack.Clear();
        _badPieces.Clear();

        //for (int i=0;i<_NUM_OF_BAD_PIECES;i++)
        //{
        //    int j = Random.Range(0, _NUM_OF_PIECES- _NUM_OF_PIECES_EXCLUDED_FROM_END);
        //    while (_badPieces.Contains(j))
        //    {
        //        j = Random.Range(0, _NUM_OF_PIECES- _NUM_OF_PIECES_EXCLUDED_FROM_END);
        //    }            
        //    _badPieces.Add(j);           
        //}

        for (int i=0;i<_blackPiecesRages.Count;i++)
        { 
            var range = _blackPiecesRages[i];           

            int startIndex = range.Key;
            int endIndex = range.Value;

            for (int j=startIndex;j<endIndex;j++)
            {
                MakeBadPiece();
            }

            if (i+1<_blackPiecesRages.Count)
            {
                for (int j = range.Value + 1; j< _blackPiecesRages[i + 1].Key; j++)
                {
                    MakeNormalPiece(j);
                }
            }
            else
            {
                for (int j = range.Value+1; j <(range.Value + 1) + pieciesBeforeLimit + _NUM_OF_PIECES_EXCLUDED_FROM_END; j++)
                {
                    MakeNormalPiece(j);
                }
            }
        }
        
        _objectsInStack.Reverse();
        _rNumOfPieces.Value = _objectsInStack.Count;
        _nextPieceToPush = 0;
        MoveCam();
        //_mainCam.transform.LookAt(_objectsInStack[0].transform);
        _onStackCreated.Invoke();       
        _lastPieceHeight = _spawnPoint.transform.position.y - _PIECE_HEIGHT/2;       
    }

    private void MakeNormalPiece(int i)
    {
        Vector3 _spPosInWorld = _spObj.transform.position;
        GameObject stackPiece = Instantiate(_stackPiece);  
        float nextPieceHeight = _lastPieceHeight + _PIECE_HEIGHT;       
        stackPiece.transform.position = new Vector3(_spPosInWorld.x, nextPieceHeight, _spPosInWorld.z);
        _lastPieceHeight = nextPieceHeight;
        _objectsInStack.Add(stackPiece);      
        float t = (float)i / (float)_NUM_OF_PIECES;
        Color pieceColor = Color.Lerp(_bottomColor, _topColor, t);
        stackPiece.GetComponent<StackPiece>().SetColor(pieceColor);       
    }

    private void MakeBadPiece()
    {
        Vector3 _spPosInWorld = _spObj.transform.position;
        GameObject stackPiece = Instantiate(_stackPiece);
        float nextPieceHeight = _lastPieceHeight + _PIECE_HEIGHT;
        stackPiece.transform.position = new Vector3(_spPosInWorld.x, nextPieceHeight, _spPosInWorld.z);
        _lastPieceHeight = nextPieceHeight;
        _objectsInStack.Add(stackPiece);
        stackPiece.GetComponent<StackPiece>().SetAsBadPiece();
    }

    private void PushNext()
    {
        if (_nextPieceToPush < _objectsInStack.Count)
        {
            StackPiece currentPiece = _objectsInStack[_nextPieceToPush].GetComponent<StackPiece>();
            currentPiece.Split();
            MoveCam();
            _rNumOfPieces.Value = _nextPieceToPush+1;
            _nextPieceToPush++;
        }
    }

    private void Disable()
    {
        _isEnabled = false;
    }

    private void Enable()
    {
        _isEnabled = true;
    }

    private void LevelColors()
    {
        Color[] colors = PickAColor();

        _bottomColor = colors[0];
        _topColor = colors[1];       
    }

    private void PickRangesInStack(out int pieciesBeforeLimit)
    {
        _lastStartIndex = 0;
        pieciesBeforeLimit = 0;
        int indexLimit = _NUM_OF_PIECES - _NUM_OF_PIECES_EXCLUDED_FROM_END;
        for (int i = 0; i < _numOfRanges; i++)
        {           
            if (_lastStartIndex + _MAX_NUM_OF_BAD_PIECES_IN_RANGE > indexLimit)
            {
                break;
            }

            int endIndex = Random.Range(_MIN_NUM_OF_BAD_PIECES_IN_RANGE, _MAX_NUM_OF_BAD_PIECES_IN_RANGE);          
            KeyValuePair<int, int> range = new KeyValuePair<int, int>(_lastStartIndex, _lastStartIndex + endIndex);
            pieciesBeforeLimit = indexLimit - (_lastStartIndex + endIndex);
            _lastStartIndex += (endIndex + Random.Range(_MIN_OFFSET_TO_NEXT_RANGE, _MAX_OFFSET_TO_NEXT_RANGE));
            _blackPiecesRages.Add(range);
        }       
    }

    private Color[] PickAColor()
    {
        int numberOfColors = _colors.height / _COLOR_OFFSET;

        int row = Random.Range(0, numberOfColors);      

        Color firstColor = _colors.GetPixel(0, row * _COLOR_OFFSET);
        Color secondColor = _colors.GetPixel(_COLOR_OFFSET, row * _COLOR_OFFSET);

        return new Color[2] { firstColor,secondColor};
    }
}

