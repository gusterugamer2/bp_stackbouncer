﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackPiece : MonoBehaviour
{
    private const float _FORCEX = 5.0f;
    private const float _FORCEY = 500f;
    private const float _TORQUE = 0.15f;  

    private Material _mat;   

    [SerializeField] private GameObject _left;
    [SerializeField] private GameObject _right;

    private bool _isBadPiece = false;

    public bool isBadPiece { get => _isBadPiece; }

    private void OnEnable()
    {
        _isBadPiece = false;
    }

    private void Awake()
    {        
        _mat = GetComponent<MeshRenderer>().material;        
    }    

    public void SetColor(Color color)
    {     
        _mat.color = color;
    }     
    
    public void SetAsBadPiece()
    {
        _isBadPiece = true;
        _mat.color = Color.black;
    }

    public void Split()
    {
        Destroy(GetComponent<MeshRenderer>());
        Destroy(GetComponent<BoxCollider>());

        _left.SetActive(true);
        _right.SetActive(true);

        _left.transform.rotation = Quaternion.Euler(0.0f, 0.0f, -10.0f);
        _right.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 10.0f);

        _left.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = _mat.color;
        _right.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = _mat.color;

        Rigidbody leftRb = _left.GetComponent<Rigidbody>();
        Rigidbody rightRb = _right.GetComponent<Rigidbody>();

        float forceX = _FORCEX / Time.fixedDeltaTime;
        float toqueZ = -_TORQUE / Time.fixedDeltaTime;       

        leftRb.AddForce(new Vector3(-forceX, _FORCEY, 0.0f));
        leftRb.AddTorque(new Vector3(0.0f, 0.0f, -toqueZ));      

        rightRb.AddForce(new Vector3(forceX, _FORCEY, 0.0f));
        rightRb.AddTorque(new Vector3(0.0f, 0.0f, toqueZ));
    }

}
