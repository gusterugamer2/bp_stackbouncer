﻿using Blastproof.Systems.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Ball : MonoBehaviour
{
    private Rigidbody _rb;
    private Camera _mainCam;
    [SerializeField] private Stack _piecesStack;
    [SerializeField] private SimpleEvent _onStackCreated;
    [SerializeField] private SimpleEvent _onReplayClick;
    [SerializeField] private SimpleEvent _onGameOver;
    [SerializeField] private SimpleEvent _onLevelCompleted;
    [SerializeField] private SimpleEvent _onStart;
    [SerializeField] private GameObject _partSyst;

    private Animator _ballAnim;

    private const float _BALL_BOUNCE_SPEED = 7.5f;
    private const float _BALL_START_OFFSET = 5.0f;

    private void Awake()
    {
        _partSyst = Resources.Load("Prefabs/DustParticlesImpact") as GameObject;
        _ballAnim = GetComponent<Animator>();
        _mainCam = Camera.main;
        _rb = GetComponent<Rigidbody>();
        _rb.isKinematic = true;
    }

    private void OnEnable()
    {
        _onStackCreated.Subscribe(Init);
        _onReplayClick.Subscribe(Init);
        _onStart.Subscribe(DisableKinematic);
    }

    private void OnDisable()
    {
        _onStackCreated.Unsubscribe(Init);
        _onReplayClick.Unsubscribe(Init);
        _onStart.Unsubscribe(DisableKinematic);
    }

    private void Init()
    {
        GameObject lastPieceOnStack = _piecesStack.objectsInStack[0];
        transform.position = lastPieceOnStack.transform.position + new Vector3(0.0f, _BALL_START_OFFSET, 0.0f);
    }

    private void DisableKinematic()
    {
        _rb.isKinematic = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        StackPiece stackPiece = collision.transform.GetComponent<StackPiece>();
        if (stackPiece && stackPiece.isBadPiece)
        {
            _onGameOver.Invoke();
        }
        else if (collision.transform.tag == "FinishLine")
        {
            _onLevelCompleted.Invoke();
            var contacts = collision.contacts;
            GameObject particle = Instantiate(_partSyst);
            particle.transform.position = contacts[0].point;
        }
        else
        {
            var contacts = collision.contacts;
            GameObject particle = Instantiate(_partSyst);
            particle.transform.position = contacts[0].point;
            _rb.velocity = new Vector3(0.0f, _BALL_BOUNCE_SPEED, 0.0f);
            _ballAnim.SetTrigger("onBallCollided");
        }
    }   
}
