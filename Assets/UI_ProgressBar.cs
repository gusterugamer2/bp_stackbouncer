﻿using Blastproof.Systems.Core.Variables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ProgressBar : MonoBehaviour
{
    [SerializeField] private FloatVariable _rProgressPerccent;

    private Image _img;
    private void Awake()
    {
        _img = GetComponent<Image>();
    }

    private void OnEnable()
    {
        _rProgressPerccent.onValueChanged += FillBar;
    }

    private void OnDisable()
    {
        _rProgressPerccent.onValueChanged -= FillBar;
    }

    private void FillBar()
    {
        _img.fillAmount = _rProgressPerccent;
    }
}
