// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Blastproof/UI/RadialGradientUI"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_ColorA("Color A", Color) = (1,1,1,1)
		_ColorB("Color B", Color) = (1,1,1,1)
		_Position("Position", Vector) = (0,0,0,0)
		_Radius("Radius", Float) = 0.5
		_Hardness("Hardness", Float) = 0.5
		_Width("Width", Float) = 0.0
		_Height("Height", Float) = 0.0

		_StencilComp("Stencil Comparison", Float) = 8
		_Stencil("Stencil ID", Float) = 0
		_StencilOp("Stencil Operation", Float) = 0
		_StencilWriteMask("Stencil Write Mask", Float) = 255
		_StencilReadMask("Stencil Read Mask", Float) = 255

		_ColorMask("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip("Use Alpha Clip", Float) = 0
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Stencil
			{
				Ref[_Stencil]
				Comp[_StencilComp]
				Pass[_StencilOp]
				ReadMask[_StencilReadMask]
				WriteMask[_StencilWriteMask]
			}

			Cull Off
			Lighting Off
			ZWrite Off
			ZTest[unity_GUIZTestMode]
			Blend One OneMinusSrcAlpha
			ColorMask[_ColorMask]

			Pass
			{
				Name "Default"
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0

				#include "UnityCG.cginc"
				#include "UnityUI.cginc"

				#pragma multi_compile_local _ UNITY_UI_CLIP_RECT
				#pragma multi_compile_local _ UNITY_UI_ALPHACLIP

				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoord : TEXCOORD0;
					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					fixed4 color : COLOR;
					float2 texcoord  : TEXCOORD0;
					float4 worldPosition : TEXCOORD1;
					half4  mask : TEXCOORD2;					
					UNITY_VERTEX_OUTPUT_STEREO
				};

				sampler2D _MainTex;
				fixed4 _Color;
				fixed4 _TextureSampleAdd;
				float4 _ClipRect;
				float4 _MainTex_ST;
				float _MaskSoftnessX;
				float _MaskSoftnessY;
				float _Width;
				float _Height;

				v2f vert(appdata_t v)
				{
					v2f OUT;
					UNITY_SETUP_INSTANCE_ID(v);
					UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
					float4 vPosition = UnityObjectToClipPos(v.vertex);
					OUT.worldPosition = v.vertex;
					OUT.vertex = vPosition;

					float2 pixelSize = vPosition.w;
					pixelSize /= float2(1, 1) * abs(mul((float2x2)UNITY_MATRIX_P, _ScreenParams.xy));

					float maxDim = max(_Width, _Height);					
					                     
					float vY = v.vertex.y / maxDim;
					float vX = v.vertex.x / maxDim;

				float4 clampedRect = clamp(_ClipRect, -2e10, 2e10);
				float2 maskUV = (v.vertex.xy - clampedRect.xy) / (clampedRect.zw - clampedRect.xy);
				OUT.texcoord = float4(vX, vY, maskUV.x, maskUV.y);
				OUT.mask = half4(v.vertex.xy * 2 - clampedRect.xy - clampedRect.zw, 0.25 / (0.25 * half2(_MaskSoftnessX, _MaskSoftnessY) + abs(pixelSize.xy)));

				OUT.color = v.color * _Color;
				return OUT;
			}

			fixed4 _ColorA;
			fixed4 _ColorB;
			fixed4 _Position;
			float _Radius;
			float _Hardness;

			fixed4 frag(v2f IN) : SV_Target
			{

				fixed4 sphereMask;
				sphereMask = 1 - saturate((distance(IN.texcoord, _Position) - _Radius) / (1 - _Hardness));

				fixed4 newColor;
				newColor = lerp(_ColorA, _ColorB, sphereMask);


				half4 color = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd) * newColor;

				color.rgb *= color.a;

				return color;
			}
		ENDCG
		}
		}
}
